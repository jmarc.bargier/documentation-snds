# KWIKLY 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Katalogue des données SNIIRAM-SNDS

::: tip
[Télécharger le Kwikly](/files/Cnam/202209_Cnam_KWIKLY-KatalogueSniiramSNDSv2022-1_MPL-2.0.xlsm) [CNAM - V2022.1 - 2022-09-02 - MPL-2.0]. 
:::


Le **Kwikly** est un fichier Excel sur plusieurs onglets décrivant les variables des produits individuels bénéficiaires (type, taille, libellé) :
- DCIR
- PMSI
- DCIRS
- Cartographie
- Causes de décès
- Référentiel Bénéficiaires
- Référentiel Médicalisé

:::tip Historique des mises à jour
- 31.07.2022 : Mise à jour (version 2022.1) avec intégration du PMSI 2021 et ajout d'une variable `Table de valeur (ORAVAL)` pour le DCIR permettant de connaître la table de valeur de telle ou telle variable sous ORAVAL (si disponible).
- 07.09.2021 : Mise à jour (version 3.2) avec l’ajout de deux nouvelles tables :
   - IR_VAC_F : Table des patients vaccinés contre le COVID-19
   - DA_PRA_R (Simplifié) : Référentiel des Professionnels de Santé (PS)
- 19.08.2021 : Mise à jour de la version 3.1 avec l’ajout des différentes tables du PMSI 2020 et de leurs variables respectives.
- 01.06.2021 : Une nouvelle version (3.1) a été mise en ligne avec notamment la table des bénéficiaires en Etablissements Médico-Sociaux IR_ESM_R et les nouvelles variables dans les tables de DCIR et des référentiels associés, dont l’identifiant cartographie.
:::

Le Kwikly permet de connaitre la présence des variables dans l’historique des produits depuis 2006.

Le Kwikly est un outil de clics rapides qui attend vos suggestions d’amélioration sur la boite <snds.cnam@assurance-maladie.fr>



