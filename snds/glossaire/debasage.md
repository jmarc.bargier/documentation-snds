# Débasage
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un débasage ou réfaction des tarifs consiste à diminuer l’enveloppe financière qui leur est dévolue, au profit d’un autre vecteur de financement (dotation <link-previewer href="MIGAC.html" text="MIGAC" preview-title="MIGAC - Missions d’intérêt général et de l’aide à la contractualisation " preview-text="La dotation de financement des MIGAC permet de financer les activités des établissements de MCO qui ne sont pas tarifées à l’activité. " /> par exemple).

# Références

- [Page wikipedia]()

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/debasage) sur le site internet du ministère
