# GHS - Groupe homogène de séjours
<!-- SPDX-License-Identifier: MPL-2.0 -->

Dans le cadre de la <link-previewer href="T2A.html" text="T2A" preview-title="T2A - Tarification à l'activité" preview-text="La tarification à l'activité (T2A) est un mode de financement des établissements de santé français issu de la réforme hospitalière du plan Hôpital 2007, qui vise à médicaliser le financement tout en équilibrant l'allocation des ressources financières et en responsabilisant les acteurs de santé. " />, le groupe homogène de séjours correspond au tarif du groupe homogène de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />). 

La très grande majorité des GHM ne correspondent qu’à un seul <link-previewer href="GHS.html" text="GHS" preview-title="GHS - Groupe homogène de séjours" preview-text="Dans le cadre de la T2A, le groupe homogène de séjours correspond au tarif du groupe homogène de malades (GHM. " />, c’est-à-dire un seul tarif. 
Mais dans certains cas, un GHM peut avoir deux ou plusieurs tarifs (dépendant, pour une même prise en charge – pour un même GHM –, de niveaux d’équipement différents, par exemple).

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/groupe-homogene-de-sejours-ghs) sur le site internet du ministère
