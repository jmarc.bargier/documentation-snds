# CMC - Catégories majeures cliniques
<!-- SPDX-License-Identifier: MPL-2.0 -->

Les Catégories majeures cliniques correspondent aux grands types de prise en charge des services de soins de suite et de réadaptation (<link-previewer href="SSR.html" text="SSR" preview-title="SSR - Soins de suite et de réadaptation" preview-text="Les soins de suite et de réadaptation (SSR) désignent un dispositif qui a pour objet de prévenir ou de réduire les conséquences fonctionnelles, physiques, cognitives, psychologiques ou sociales des déficiences et des limitations de capacité des patients et de promouvoir leur réadaptation et leur réinsertion. " />) : cardiovasculaire et respiratoire, neuromusculaire, post-traumatique, soins palliatifs, etc.

Les CMC structurent la classification employée dans le cadre du <link-previewer href="PMSI.html" text="PMSI" preview-title="PMSI - Programme de médicalisation des systèmes d’information" preview-text="Le PMSI permet de décrire de façon synthétique et standardisée l’activité médicale des établissements de santé. Il repose sur l’enregistrement de données médico-administratives normalisées dans un recueil standard d’information. Il comporte 4 « champs » : « médecine, chirurgie, obstétrique et odontologie » (MCO) « soins de suite ou de réadaptation » (SSR) « psychiatrie » sous la forme du RIM-Psy (recueil d’information médicale en psychiatrie) « hospitalisation à domicile » (HAD)" />. 

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/categories-majeures-cliniques-cmc) sur le site internet du ministère
