# Ex-DG - ex dotation globale
<!-- SPDX-License-Identifier: MPL-2.0 -->

Néologisme désignant les établissements financés par la dotation globale, avant la mise en place de la <link-previewer href="T2A.html" text="tarification à l’activité" preview-title="T2A - Tarification à l'activité" preview-text="La tarification à l'activité (T2A) est un mode de financement des établissements de santé français issu de la réforme hospitalière du plan Hôpital 2007, qui vise à médicaliser le financement tout en équilibrant l'allocation des ressources financières et en responsabilisant les acteurs de santé. " />[^1].

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/ex-dg-ex-dotation-globale) sur le site internet du ministère

[^1] : Établissements visés aux alinéas a, b et c de l’article L. 162-22-6 du code de la sécurité sociale dont les établissements publics de santé et les établissements privés à but non lucratif participant au service public hospitalier