# OQN - Objectif quantifié national
<!-- SPDX-License-Identifier: MPL-2.0 -->

cf <link-previewer href="ONDAM.html" text="ONDAM" preview-title="ONDAM - Objectif national des dépenses d’assurance maladie" preview-text="L’ONDAM est fixé chaque année par le Parlement, conformément aux dispositions de la loi de financement de la sécurité sociale (LFSS. " />

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/oqn-objectif-quantifie-national-cf-ondam) sur le site internet du ministère
