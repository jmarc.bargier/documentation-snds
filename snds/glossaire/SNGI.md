# SNGI - Système national de gestion des identifiants
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le Système national de gestion des identifiants (SNGI) répertorie l'état civil et le <link-previewer href="NIR.html" text="NIR" preview-title="NIR - Numéro de sécurité sociale" preview-text="Le numéro d'inscription au répertoire (NIR) est le numéro d'inscription au répertoire national d'identification des personnes physiquesRNIPP." /> des personnes relevant d’un régime de sécurité sociale. 
Il est géré par la Caisse nationale d'assurance vieillesse (<link-previewer href="Cnav.html" text="CNAV" preview-title="Cnav - Caisse nationale d'assurance vieillesse" preview-text="La Caisse nationale d'assurance vieillesse (Cnav) gère la retraite des salariés, hors secteur agricole et hors fonction publique." />), pour qui il sert de référence en matière d'identités.

Le SNGI a la charge :
- d'attribuer le NIR des personnes nées à l'étranger qui viennent travailler en France ;
- de diffuser l'état civil et le NIR aux <link-previewer href="OPS.html" text="organismes de protection sociale" preview-title="OPS - Organisme de protection sociale " preview-text="Aucune définition détaillée n'existe pour l'instant dans le glossaire. Pour contribuer, référrez-vous au guide de contribution." /> : <link-previewer href="MSA.html" text="MSA" preview-title="MSA - Mutualité sociale agricole" preview-text="La Mutualité sociale agricole (MSA) est le régime de protection sociale obligatoire des personnes salariées et non salariées des professions agricoles." />, <link-previewer href="Cnaf.html" text="CNAF" preview-title="CNAF - Caisse nationale des allocations familiales" preview-text="La Caisse nationale des allocations familiales (CNAF) forme la branche « famille » de la Sécurité sociale française, qu'elle gère au travers le réseau formé par les 102 caisses d'allocations familiales (CAF) réparties sur tout le territoire." />, <link-previewer href="Cnam.html" text="CNAM" preview-title="CNAM - Caisse nationale de l’assurance maladie" preview-text="La Caisse nationale de l’assurance maladie est la « tête de réseau » opérationnelle du régime d’assurance maladie obligatoire en France. " />, <link-previewer href="retraite_complementaire_salaries.html" text="AGIRC-ARRCO" preview-title="Retraite complémentaire des salariés " preview-text="La retraite complémentaire des salariés est une retraite qui complète la retraite de base de la CNAV." />, <link-previewer href="CFE.html" text="CFE" preview-title="CFE - Caisse des Français de l'étranger " preview-text="La Caisse des Français de l’étranger (CFE) est une caisse de Sécurité sociale à adhésion volontaire." />, etc. 

Le SNGI est également consulté par les Maisons départementales des personnes handicapées (<link-previewer href="MDPH.html" text="MDPH" preview-title="MDPH - Maison Départementale des Personnes Handicapées " preview-text="Les Maisons départementales des personnes handicapées (MDPH) furent créées par la loi n° 2005-102 du 11 février 2005. " />). 

## Contenu

Le SNGI contient des informations sur l'état civil :
- nom patronymique ;
- liste des prénoms ;
- date et lieu de naissance ;
- filiation : nom et prénoms du père et de la mère ;
- date et lieu de décès ;
- identités secondaires : maritaux, noms d’usage ;
- historiques des modifications : NIR, identités.

Il contient également des informations sur la mutation vieillesse : 
- le code organisme ;
- le numéro interne de l’organisme partenaire ;
- la date d’obtention du numéro.

## Alimentation 

Le SNGI est alimenté quotidiennement par le <link-previewer href="RNIPP.html" text="RNIPP" preview-title="RNIPP - Répertoire national d'identification des personnes physiques" preview-text="Le répertoire national d'identification des personnes physiques (RNIPP) est un répertoire français, tenu par l'Insee depuis 1946. " /> pour les personnes nés en France.

A l'inverse, le SNGI alimente le RNIPP pour les personnes nés hors de France.

## Utilisations

Le SNGI offre plusieurs services :
- identification des assurés ;
- affiliation des assurés à un organisme ;
- immatriculation des assurés nés hors de France ;
- notification à un organisme des modifications intervenant sur un assuré.

Le SNGI permet également de rechercher un assuré à partir d’éléments incomplets ou partiellement erronés, afin d'obtenir son NIR et son état civil.

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_national_de_gestion_des_identifiants) 
- [Contenu du SNGI](http://www.identito-vigilance.org/JNIV2009/Presentations_files/JNIV2009-cnf2.pdf#page=10)
